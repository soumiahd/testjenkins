<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Reservation</title>
<link href="fichiers/page1.css" rel="stylesheet">
</head>
<body>
	<div class="super">
		<div class="corps">
			<div class="entete">
				<h1>Groupe CVE - R�servation</h1>
				<div class="phototexte">
					<img class="photo" src="fichiers/photo.jpg">
					<div class="texte">
						<p>Ce site est un service qui vous permet de reserver en ligne
							et en temps r�el, votre s�jour dans un camping</p>
						<p>Simple et s�curitaire : ce site est la solution facile pour
							planifier vos vacances ou vos week-ends dans la r�gion de votre
							choix</p>
					</div>
				</div>
				<form method="Post" action="formulaire">
					<div class="formulaire">
						<div class="zone1">
							<div class="region">
								<p>
									<em>1 </em> CHOISIR UNE REGION :
								</p>
								<form class="listeregion">
									<select size="1">
										<optgroup label="Toutes les r�gions">
											<option>TOUTES LES REGIONS</option>
											<option>BRETAGNE</option>
											<option>NORMANDIE</option>
											<option>HAUTS DE FRANCE</option>
											<option>ILE-DE-FRANCE</option>
											<option>GRAND EST</option>
											<option>PAYS DE LOIRE</option>
											<option>CENTRE - VAL DE LOIRE</option>
											<option>BOURGOGNE-FRANCHE-COMTE</option>
											<option>NOUVELLE AQUITAINE</option>
											<option>AUVERGNE RH�NE-ALPES</option>
											<option>OCCITANIE</option>
											<option>PROVENCE-ALPES-C�TE-D'AZUR</option>
											<option>CORSE</option>
										</optgroup>
									</select>
								</form>
							</div>
							<form class="dateArrivee">
								<p>
									<em>2 </em> DATE D'ARRIVEE :
								</p>
								<input id="date" type="date" />
							</form>


							<form class="nuitee">
								<p>
									<em>3 </em> NOMBRE DE NUITEE :
								</p>
								<input id="nuitee" type="number" name="quantity" min="0"
									max="100" value="1" input type="niveau" />
							</form>


							<form class="servicePreference">
								<div class="service">
									<p>
										<em>4 </em> SERVICES :
									</p>
									<div class="listeservice">
										<p>
											<input type="radio" name="service" value="1"></input>Sans
											service
										</p>
										<p>
											<input type="radio" name="service" value="2">Eau
											seulement</input>
										</p>
										<p>
											<input type="radio" name="service" value="3">Eau -
											electricit�</input>
										</p>
										<p>
											<input type="radio" name="service" value="4">Eau -
											electricit� - egouts</input>
										</p>
										<p>
											<input type="radio" name="service" value="5">Tous les
											types de services</input>
										</p>
									</div>
								</div>
								<div class="preference">
									<p>
									<h2></h2>
									PREFERENCE :
									</p>
									<div class="listepreference">
										<select size="1">
											<optgroup label="Choix d'amp�rage">
												<option>2A</option>
												<option>3A</option>
												<option>4A</option>
												<option>5A</option>
												<option>6A</option>
												<option>10A</option>
												<option>20A</option>
											</optgroup>
										</select>
										<p>
											<input type="radio" name="service" value="6">Acces
											direct (pull through)</input>
										</p>
									</div>
								</div>
							</form>
						</div>
						<div class="zone2">


							<form class="equipement">
								<p>
									<em>5 </em> TYPE D'EQUIPEMENT :
								</p>
								<div class="listeequipement">
									<p>
										<input type="radio" name="equipement" value="1">Tente</input>
									</p>
									<p>
										<input type="radio" name="equipement" value="2">Tente
										- roulotte</input>
									</p>
									<p>
										<input type="radio" name="equipement" value="3">Roulotte</input>
									</p>
									<p>
										<input type="radio" name="equipement" value="4">Roulotte
										hybride</input>
									</p>
									<p>
										<input type="radio" name="equipement" value="5">Caravane
										a sellette</input>
									</p>
									<p>
										<input type="radio" name="equipement" value="6">Caravane
										portee</input>
									</p>
									<p>
										<input type="radio" name="equipement" value="7">Autocaravane-Motorise-Camping
										car</input>
									</p>
									<p>
										<input type="radio" name="equipement" value="8">Pret-a-camper(tentes,roulottes
										ou yourtes)</input>
									</p>
									<p>
										<input type="radio" name="equipement" value="9">Location
										de chalet</input>
									</p>
								</div>
							</form>
							<form class="longueurEquipement">
								<p>
									<em>6 </em> LONGUEUR D'EQUIPEMENT :
								</p>
								<div class="listeLongueurEquipement">
									<select size="1">
										<optgroup label="Choix de la longueur">
											<option>3 m�tres</option>
											<option>4 m�tres</option>
											<option>5 m�tres</option>
											<option>6 m�tres</option>
										</optgroup>
									</select>
								</div>
							</form>
							<form>
								<p class="label">Svp,veuillez vous assurer que la longueur
									selectionnee reflete bien la longueur totale (hors-tout) de
									votre �quipement.</p>
								<button onclick="bouton()">D�buter la recherche</button>
							</form>

						</div>
					</div>
				</form>
			</div>



		</div>
</body>
</html>