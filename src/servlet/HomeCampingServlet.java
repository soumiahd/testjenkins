package servlet;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomeCampingServlet extends HttpServlet{
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		IFilDeDiscussionDao filDeDiscussionDao = DaoFactory.getFilDeDiscussionDao();
//		List<FilDeDiscussion> liste = filDeDiscussionDao.liste();
//		
//		req.setAttribute("filsDeDiscussion", liste);
		
		req.getRequestDispatcher("/WEB-INF/jsp/page11.jsp").forward(req, resp);
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
	
}