package model;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Sejour {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String numero;
	@OneToOne
	private Facture facture;
	@OneToOne
	private Groupe groupe;
	@OneToMany(mappedBy="sejour")
	private List<Nuitee> nuitees;
	@OneToMany(mappedBy="sejour")
	private List<Activite> activites;
	
	public Sejour() {}
	
	public Facture getFacture() {
		return facture;
	}

	public void setFacture(Facture facture) {
		this.facture = facture;
	}

	public Groupe getGroupe() {
		return groupe;
	}

	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}

	public List<Nuitee> getNuitees() {
		return nuitees;
	}

	public void setNuitees(List<Nuitee> nuitees) {
		this.nuitees = nuitees;
	}

	public List<Activite> getActivites() {
		return activites;
	}

	public void setActivites(List<Activite> activites) {
		this.activites = activites;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
}
