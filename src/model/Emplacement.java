package model;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)

public abstract class Emplacement {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private int surface;
	private int capacite;
	
	private String numero;
	protected float prix;
	
	@ManyToOne
	private Camping camping;
	
	@OneToMany(mappedBy="emplacement")
	private List<Nuitee> nuitees;
	
	
	
	public Emplacement() {}
	
	public Camping getCamping() {
		return camping;
	}


	public void setCamping(Camping camping) {
		this.camping = camping;
	}


	public List<Nuitee> getNuitees() {
		return nuitees;
	}


	public void setNuitees(List<Nuitee> nuitees) {
		this.nuitees = nuitees;
	}


	public int getSurface() {
		return surface;
	}
	public void setSurface(int surface) {
		this.surface = surface;
	}
	public int getCapacite() {
		return capacite;
	}
	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
}
