package dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.query.Query;

import model.Camping;
import util.HibernateUtil;

public class DaoCamping {
	public Camping getCampingByNom(String nom) {
		
		Session session = HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Camping> criteria = criteriaBuilder.createQuery(Camping.class);
		Root<Camping> root = criteria.from(Camping.class);
		criteria.where(criteriaBuilder.equal(root.get("nom"), nom));
		Query<Camping> query = session.createQuery(criteria);
		
		List<Camping> resultList = query.getResultList();
		
		if (resultList.size() > 0) return resultList.get(0);
		return null;
		
	}
}
