package dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.query.Query;

import model.Emplacement;
import util.HibernateUtil;

public class DaoEmplacement {
	public Emplacement getEmplacementByNumero(String numero) {

		Session session = HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Emplacement> criteria = criteriaBuilder.createQuery(Emplacement.class);
		Root<Emplacement> root = criteria.from(Emplacement.class);
		criteria.where(criteriaBuilder.equal(root.get("numero"), numero));
		Query<Emplacement> query = session.createQuery(criteria);

		List<Emplacement> resultList = query.getResultList();

		if (resultList.size() > 0) return resultList.get(0);
		return null;

	}
}
