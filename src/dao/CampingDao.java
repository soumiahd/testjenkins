package dao;
import model.Camping;

public class CampingDao extends GenericDao<Camping> implements ICampingDao{

	public CampingDao() {
		super(Camping.class);
	}

}
